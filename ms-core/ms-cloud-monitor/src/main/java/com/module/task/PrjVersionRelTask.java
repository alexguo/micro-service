package com.module.task;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.module.admin.prj.pojo.PrjVersion;
import com.module.admin.prj.service.PrjVersionService;
import com.module.admin.prj.utils.PrjClientUtil;
import com.system.comm.utils.FrameNoUtil;
import com.system.comm.utils.FrameSpringBeanUtil;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * 发布项目版本的任务
 * @author yuejing
 * @date 2016年10月22日 上午9:58:59
 * @version V1.0.0
 */
public class PrjVersionRelTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(PrjVersionRelTask.class);
	
	/**
	 * @param initialDelay	初始执行延迟时间（单位:秒）
	 * @param period		执行间隔（单位:秒）
	 */
	public PrjVersionRelTask run(int initialDelay, int period) {
		LOGGER.info("========================= 发布项目版本的任务 - 成功 ===========================");
		ScheduledExecutorService service = Executors.newScheduledThreadPool(20);
		final PrjVersionService prjVersionService = FrameSpringBeanUtil.getBean(PrjVersionService.class);
		//线程，每隔5秒调用一次
		Runnable runnable = new Runnable() {
			public void run() {
				//执行发布版本到服务器
				String relMsg = "服务编码" + FrameNoUtil.uuid() + "发布中";
				prjVersionService.updateRelTimeWait(relMsg);
				List<PrjVersion> pvs = prjVersionService.findRelTime(relMsg);
				for (PrjVersion pv : pvs) {
					ResponseFrame frame = PrjClientUtil.releaseAll(pv.getPrjId(), pv.getVersion());
					if(ResponseCode.SUCC.getCode() != frame.getCode().intValue()) {
						LOGGER.info("项目[" + pv.getPrjId() + "]的版本[" + pv.getVersion() + "]发布失败: " + frame.getMessage());
						prjVersionService.updateRelTimeStatus(pv.getPrjId(), pv.getVersion(), 30, FrameTimeUtil.getStrTime() + "发布失败");
						//回滚到上一个版本
						ResponseFrame rbFrame = PrjClientUtil.releaseAll(pv.getPrjId(), pv.getRbVersion());
						if(ResponseCode.SUCC.getCode() != rbFrame.getCode().intValue()) {
							LOGGER.info("项目[" + pv.getPrjId() + "]回滚到版本[" + pv.getVersion() + "]失败: " + rbFrame.getMessage());
						}
					} else {
						LOGGER.info("项目[" + pv.getPrjId() + "]的版本[" + pv.getVersion() + "]发布成功: " + frame.getMessage());
						//修改定时的发布状态
						prjVersionService.updateRelTimeStatus(pv.getPrjId(), pv.getVersion(), 40, FrameTimeUtil.getStrTime() + "发布成功");
					}
				}
				/*List<MsSecret> list = secretService.findUse();
				for (MsSecret msSecret : list) {
					AuthUtil.updateAuthClient(new AuthClient(msSecret.getCliId(), msSecret.getName(),
							msSecret.getDomain(), msSecret.getToken(), msSecret.getDomain()));
				}*/
			}
		};
		// 第二个参数为首次执行的延时时间，第三个参数为定时执行的间隔时间  
		service.scheduleAtFixedRate(runnable, initialDelay, period, TimeUnit.SECONDS);
		return this;
	}

}

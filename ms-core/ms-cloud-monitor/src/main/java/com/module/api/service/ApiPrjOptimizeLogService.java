package com.module.api.service;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.system.handle.model.ResponseFrame;

@Component
public interface ApiPrjOptimizeLogService {

	public abstract ResponseFrame save(String code, String url, String params,
			Date reqTime, Date resTime, Integer useTime, String resResult, String remark);

}

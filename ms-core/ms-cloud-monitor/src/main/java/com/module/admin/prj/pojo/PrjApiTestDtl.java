package com.module.admin.prj.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * prj_api_test_dtl实体
 * @author autoCode
 * @date 2018-03-08 10:59:40
 * @version V1.0.0
 */
@Alias("prjApiTestDtl")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class PrjApiTestDtl extends BaseEntity implements Serializable {
	//项目API测试编号
	private Integer patId;
	//项目API路径
	private String path;
	//参数
	private String params;
	//状态[10待测试、20测试中、30成功、40失败]
	private Integer status;
	//成功条件
	private String succCond;
	//创建时间
	private Date createTime;
	//测试时间
	private Date testTime;
	//测试结果
	private String testResult;
	
	//=============================== 扩展属性
	//状态名称
	private String statusName;
	
	public Integer getPatId() {
		return patId;
	}
	public void setPatId(Integer patId) {
		this.patId = patId;
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public String getParams() {
		return params;
	}
	public void setParams(String params) {
		this.params = params;
	}
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getSuccCond() {
		return succCond;
	}
	public void setSuccCond(String succCond) {
		this.succCond = succCond;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public Date getTestTime() {
		return testTime;
	}
	public void setTestTime(Date testTime) {
		this.testTime = testTime;
	}
	
	public String getTestResult() {
		return testResult;
	}
	public void setTestResult(String testResult) {
		this.testResult = testResult;
	}
}
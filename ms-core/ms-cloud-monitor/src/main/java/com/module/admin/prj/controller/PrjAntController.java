package com.module.admin.prj.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.module.admin.BaseController;
import com.module.admin.prj.pojo.PrjAnt;
import com.module.admin.prj.service.PrjAntService;
import com.module.admin.prj.service.PrjInfoService;
import com.system.comm.model.KvEntity;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * prj_api的Controller
 * @author yuejing
 * @date 2016-11-30 13:30:00
 * @version V1.0.0
 */
@Controller
public class PrjAntController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PrjAntController.class);

	@Autowired
	private PrjAntService prjAntService;
	@Autowired
	private PrjInfoService prjInfoService;

	/**
	 * 跳转到管理页
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/prjAnt/f-view/manager")
	public String manger(HttpServletRequest request, ModelMap modelMap) {
		//获取所有服务
		List<KvEntity> prjInfos = prjInfoService.findKvAll();
		modelMap.put("prjInfos", prjInfos);
		return "admin/prj/ant-manager";
	}

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/prjAnt/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			PrjAnt prjAnt) {
		ResponseFrame frame = null;
		try {
			frame = prjAntService.pageQuery(prjAnt);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	/**
	 * 跳转到编辑页[包含新增和编辑]
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/prjAnt/f-view/edit")
	public String edit(HttpServletRequest request, ModelMap modelMap, Integer paId) {
		//获取所有服务
		List<KvEntity> prjInfos = prjInfoService.findKvAll();
		modelMap.put("prjInfos", prjInfos);
		if(paId != null) {
			modelMap.put("prjAnt", prjAntService.get(paId));
		}
		return "admin/prj/ant-edit";
	}

	/**
	 * 保存
	 * @return
	 */
	@RequestMapping(value = "/prjAnt/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response,
			PrjAnt prjAnt) {
		ResponseFrame frame = null;
		try {
			frame = prjAntService.saveOrUpdate(prjAnt);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	@RequestMapping(value = "/prjAnt/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response,
			Integer paId) {
		ResponseFrame frame = null;
		try {
			frame = prjAntService.delete(paId);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}

	@RequestMapping(value = "/prjAnt/f-view/func")
	public String func(HttpServletRequest request, ModelMap modelMap, Integer paId) {
		if(paId != null) {
			modelMap.put("prjAnt", prjAntService.get(paId));
		}
		return "admin/prj/ant-func";
	}
}